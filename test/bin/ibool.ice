// -*- mode: c++; coding: utf-8 -*-

module DUO {
    module IBool {
	interface W {
	    void set(bool state, string source);
	};
    };
};
